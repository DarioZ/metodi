#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// funzione da integrare
float f(float x){
    return exp(x);
}

// derivata seconda di f (per calcolo errore Trapezi)
float f2(float x){
    return exp(x);
}

// derivata quarta di f (per calcolo errore Simpson)
float f4(float x){
    return exp(x);
}

// valore massimo di una f data tra a e b, calcolato per n intervalli
float maxf(float (*f)(float), float a, float b, int n) {
    int i;
    float max = f(a);
    float h = (b-a)/n;
    for(i=1;i<=n;i++) {
        if(max<f(a+i*h)) max=f(a+i*h);
    }
    return max;
}

// ritorna array con valore ed errore dell'integrale (trapezi)
// argomenti: f(x) da integrare, derivata seconda f2(x) (per calcolo errore), intervallo di integrazione a, b, numero n di intervalli
float* trapezi(float (*f)(float), float (*f2)(float), float a, float b, int n){
    int i;
    float* I = malloc(2 * sizeof(float)); // array da ritornare
    float h = (b-a)/n;
    I[0] = f(a)/2 + f(b)/2; // sommo i trapezi
    for(i=1;i<n;i++) {
        I[0] += f(a + i*h);
    }
    I[0] *= h;
    I[1] = fabs(maxf(f2,a,b,n)*(b-a)*h*h/12); // errore
    return I;
}

// ritorna array con valore ed errore dell'integrale (simpson)
// argomenti: f(x) da integrare, derivata quarta f4(x) (per calcolo errore), intervallo di integrazione a, b, numero n di intervalli
float* compositeSimpson(float (*f)(float), float(*f4)(float), float a, float b, int n){
    int i;
    float* I = malloc(2 * sizeof(float));
    n += (n%2)?1:0; // voglio che n sia pari
    float h = (b-a)/n;

    I[0] = f(a) + f(b);
    for(i=1;i<n;i++) {
        I[0] += (i%2)?4*f(a+i*h):2*f(a+i*h);
    }
    I[0] *= h/3;
    I[1] = fabs((b-a)*maxf(f4, a, b, n)*pow(h,4)/180); // errore
    
    return I;
}

int main(int argc, char** argv){
    float a = atof(argv[1]);
    float b = atof(argv[2]);
    int n = atoi(argv[3]);
    n += (n%2)?1:0; // voglio che n sia pari

    float* I_T = trapezi(f, f2, a, b, n);
    float* I_S = compositeSimpson(f, f4, a, b, n);

    printf("Metodo dei trapezi: I = %f +- %f\n", I_T[0], I_T[1]);
    printf("Metodo di Simpson:  I = %f +- %f\n", I_S[0], I_S[1]);
    return 0;
}
