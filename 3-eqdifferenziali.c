#include<stdio.h>
#include<stdlib.h>
#include<math.h>

// funzione dell'equazione differenziale nella forma dy/dx = f(x,y)
float f(float x, float y) {
    return (1 - x - 2*x*x)*y/(x + 0.5);
}

/* ritorna il valore della soluzione y(x) dell'equazione differenziale in forma dy/dx = f(x,y)
 * nel punto xf, data una condizione al contorno y(x0) = y0, e dato un numero di passi n
 */
float rungeKutta4(float (*f)(float x, float y), float xf, float x0, float y0, int n) {
    int i;
    float k0, k1, k2, k3;
    float h = (xf-x0)/n;

    for(i=0;i<n;i++) {
        k0 = h*f(x0,y0);
        k1 = h*f(x0 + h/2, y0 + k0/2);
        k2 = h*f(x0 + h/2, y0 + k1/2);
        k3 = h*f(x0 + h, y0 + k2);
        y0 += (k0 + 2*k1 + 2*k2 + k3)/6;
        x0 += h;
    }

    return y0;
}

int main(int argc, char** argv) {
    float xf = atof(argv[1]);
    float x0 = atof(argv[2]);
    float y0 = atof(argv[3]);
    int n = atoi(argv[4]);

    printf("Condizione al contorno:\ty(%f)=%f\n", x0, y0);
    printf("Soluzione dell'equazione differenziale in %d passi nel punto xf scelto\n", n);
    printf("x = %f\ty(x) = %f\n", xf, rungeKutta4(f,xf,x0,y0,n));

    return 0;
}