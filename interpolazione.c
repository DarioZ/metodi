#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include"utils/readfiles.h"
//gcc -o interpolazione.exe interpolazione.c utils/readfiles.o -lm

// Using algorithm on Murphy to calculate forward Gregory-Newton
// returns value of the formula on point valx, up to order order
float forwardGNMurphy(float valx, int order, float* x, float* f, size_t n) {
    float h, r, coeff, yx;
    int i, j;
    float* diff = malloc(order * sizeof(float));

    h = (x[n-1]-x[0])/(n-1);
    // find nearest value x[j] to calculate formula
    j = round((valx - x[0])/h - 0.5);
    r = (valx-x[0])/h - j;

    if((j>=0)&&(j<n-order)) {
        // calcolo le differenze al 1 ordine
        for(i=0;i<order;i++) diff[i]=f[i+j+1] - f[i+j];
        coeff=r;
        yx = f[j] + coeff*diff[0];
        // calcolo gli ordini successivi
        for(i=2;i<=order;i++) {
            coeff *= (r-i+1)/i;
            for(j=0;j<=(order-i);j++) {
                diff[j] = diff[j+1] - diff[j];
                yx += coeff*diff[0];
            }
        }
    }
    else {
        fprintf(stderr, "%d valori insufficienti per calcolare la formula all'ordine %d\n", n, order);
        exit(EXIT_FAILURE);
    }

    return yx;
}

// Calcolo delta secondo la formula
float delta(float* f, int j, int k, size_t n) {
    int r;
    float c=1;
    float delta;

    if(j>=0 && (j+k)<n) {
        delta = f[j+k];
        for(r=1;r<=k;r++) {
            c *= (float) -(k-r+1)/r;
            delta += c*f[j+k-r];
        }

    } else {
        fprintf(stderr, "%d valori insufficienti per calcolare delta all'ordine %d\n", n, k);
        exit(EXIT_FAILURE); 
    }

    return delta;
}

// Calculate forward Gregory-Newton formula
// returns value of the formula on point valx, up to order order
// it is 0.3% faster than forwardGNMurphy and cleaner, I think
float forwardGN(float valx, int order, float* x, float* f, size_t n) {
    float h, r, coeff, yx;
    int i,j;

    // find nearest value x[j] to calculate formula, interval h, spacing r
    h = (x[n-1]-x[0])/(n-1);
    j = round((valx - x[0])/h - 0.5);
    r = (valx-x[0])/h - j;

    // calcola la formula all'ordine dato come sum(c*delta^k)
    coeff = 1;
    yx = f[j];
    for(i=1;i<=order;i++) {
        coeff *= (r - i + 1)/i;
        yx += coeff*delta(f, j, i, n);
    }

    return yx;
}

int main(int argc, char** argv){
    char* filename = "values.txt";
    float valx = 0.27;
    int order = 2;
    int i;
    float yx;

    size_t n;
    float** values = toXYArray(filename, &n);

    float* x = values[0];
    float* f = values[1];
    free(values);

    yx = forwardGN(valx, order, x, f, n);
    printf("%f\n", yx);

    size_t rows, cols;
    values = toArray(filename, &rows, &cols);
    x = extractCol(values, rows, cols, 0);
    f = extractCol(values, rows, cols, 1);
    free(values);
    
    yx = forwardGN(valx, order, x, f, rows);
    printf("%f\n", yx);

    return 0;
}