#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**
 * Implementazione del metodo di Newton-Raphson per la risoluzione di equazioni non lineari.
 * Il codice risolve x³+2x+1=0
 * attraverso la funzione g(x)=x-f/f' costruita appositamente.
 */


// Funzione generatrice di Newton-Raphson per x³+2x+1=0
// f è funzione, f1 è derivata, ritorna x-f/f1
float g(float x){
    float f = pow(x, 3) + 2*x + 1;
    float f1 = 3*pow(x,2) + 2;
    float val = x -  f/f1;
    return val;
}

int main(int argc, char **argv){
    // Usage: enter x0, eps to start the program
    
    // Il programma accetta valori da linea di comando, convertiti con atof (da stdlib)

    // float x0, eps;
    // printf("Inserisci valore di partenza x0 e errore epsilon\n");
    // scanf("%f%f", &x0, &eps);

    int i=0;
    float x;
    float x0 = atof(argv[1]);
    float eps = atof(argv[2]);

    printf("Risolvo x³+2x+1=0\n\n");
    printf("x0 = %f\neps = %f\n\n", x0, eps);

    // Ciclo di iterazioni x_(n+1) = g(x_n) finché non è |x_(n+1) - x_n|<eps
    do
    {
        i++;
        x = x0;
        x0 = g(x);
    } while (fabs(x-x0)>eps);

    printf("Il valore dello zero (iterazione %d) è x = (%f +- %f)\n", i, x0, fabs(x-x0));
    return 0;
}