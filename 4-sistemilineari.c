#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"utils/readfiles.h"
//gcc -o 4-sistemilineari.exe 4-sistemilineari.c utils/readfiles.o -lm

/* Ritorna il massimo di un array
 * di n valori.
 */
float max(float* arr, size_t n) {
    int i;
    float max = arr[0];
    for(i=1;i<n;i++) {
        if(max<arr[i]) max=arr[i];
    }
    return max;
}

/* Stampa un array di float come %.decf\t,
 * con dec cifre decimali
 */
void printArray(float* arr, size_t n, int dec) {
    int i;
    for(i=0;i<n;i++) {
        printf("%.*f\t", dec, arr[i]);
    }
    printf("\n");
}

/* Fattorizzazione LU di una matrice nxn.
 * Scrive le matrici su float** L e float** U, che devono essere
 * precedentemente allocate.
 */
void LUfact(float** A, float** L, float** U, size_t n) {
    int i, j, k;

    for(i=0;i<n;i++) {
        U[i][i] = A[i][i];
        for(k=0;k<i;k++) U[i][i] -= L[i][k]*U[k][i];
        L[i][i] = 1;
        for(j=(i+1);j<n;j++) {
            U[j][i] = 0;
            L[i][j] = 0;
            U[i][j] = A[i][j];
            L[j][i] = A[j][i];
            for(k=0;k<i;k++) {
                U[i][j] -= L[i][k]*U[k][j];
                L[j][i] -= L[j][k]*U[k][i];
            }
            L[j][i] /= U[i][i];
        }
    }
}

/* Risolve sistema triangolare superiore Ux=b con algoritmo
 * di sostituzione all'indietro.
 * Ritorna il vettore float* x soluzione del sistema.
 */
float* backwardSub(float** U, float* b, size_t n) {
    int i, k;
    float* x = malloc(n*sizeof(float));
    
    for(i=(n-1);i>=0;i--) {
        x[i] = b[i];
        for(k=(i+1);k<n;k++) x[i] -= U[i][k]*x[k];
        x[i] /= U[i][i];
    }

    return x;
}

/* Risolve sistema triangolare inferiore Lx=b con algoritmo
 * di sostituzione in avanti.
 * Ritorna il vettore float* x soluzione del sistema.
 */
float* forwardSub(float** L, float* b, size_t n) {
    int i, k;
    float* x = malloc(n*sizeof(float));

    for(i=0;i<n;i++) {
        x[i] = b[i];
        for(k=0;k<i;k++) x[i] -= L[i][k]*x[k];
        x[i] /= L[i][i];
    }

    return x;
}

/* Risolve sistema lineare con metodo iterativo di Gauss-Seidel
 * fino alla tolleranza eps.
 * Ritorna il vettore float* x soluzione del sistema.
 */
float* gaussSolve(float** A, float* b, size_t n, float eps) {
    int i, j, k=0;
    float* x = calloc(n, sizeof(float));
    float xprec;
    float* diffs = calloc(n, sizeof(float));

    do {
        for(i=0;i<n;i++) {
            xprec = x[i];
            x[i] = b[i];
            for(j=0;j<i;j++) x[i] -= A[i][j]*x[j]; //x[j] è all'iterazione corrente
            for(j=(i+1);j<n;j++) x[i] -= A[i][j]*x[j];
            x[i] /= A[i][i];
            diffs[i] = fabs(x[i] - xprec);
        }
        k++;
    } while(max(diffs, n)>eps);

    printf("Gauss-Seidel risolto in %d iterazioni\n", k);
    return x;
}

/* Risolve sistema lineare con metodo iterativo di Jacobi
 * fino alla tolleranza eps.
 * Ritorna il vettore float* x soluzione del sistema.
 */
float* jacobiSolve(float** A, float* b, size_t n, float eps) {
    int i, j, k;
    float* x = calloc(n, sizeof(float));
    float* xprec = calloc(n, sizeof(float));
    float* diffs = calloc(n, sizeof(float));

    do {
        for(i=0;i<n;i++) xprec[i] = x[i];
        for(i=0;i<n;i++) {
            x[i] = b[i];
            for(j=0;j<i;j++) x[i] -= A[i][j]*xprec[j];
            for(j=(i+1);j<n;j++) x[i] -= A[i][j]*xprec[j];
            x[i] /= A[i][i];
            diffs[i] = fabs(x[i] - xprec[i]);
        }
        k++;
    } while(max(diffs, n)>eps);

    printf("Jacobi risolto in %d iterazioni\n", k);
    return x;
}

int main(int argc, char**argv) {
    char* filename = argv[1];
    int i,j;
    size_t rows, cols, n;
    
    
    float** Ab = toArray(filename, &rows, &cols);    
    printf("%d\t%d\n", rows, cols);
    if(rows!=(cols-1)) {
        fprintf(stderr, "Non posso risolvere un sistema non quadrato.\n");
        exit(EXIT_FAILURE);
    }
    n = rows;
    float* b  = extractCol(Ab, rows, cols, (cols-1)); 
    
    for(i=0;i<n;i++) {
            printArray(Ab[i], n+1, 6);
    }
    

    // Risolvo il sistema con la fattorizzazione LU
    float** U = calloc(n, sizeof(float*));
    float** L = calloc(n, sizeof(float*));
    for(i=0;i<n;i++) {
        U[i] = calloc(n, sizeof(float));
        L[i] = calloc(n, sizeof(float));
    }
    LUfact(Ab, L, U, n);
    float* z = forwardSub(L, b, n);
    float* x = backwardSub(U, z, n);

    printf("Soluzione con metodo di fattorizzazione LU:\n");
    printArray(x, 4, 4);

    // Risolvo il sistema con i metodi iterativi
    printf("Soluzione con metodo di Jacobi:\n");
    x = jacobiSolve(Ab, b, n, 0.0001);
    printArray(x, 4, 4);
    printf("Soluzione con metodo di Gauss-Seidel:\n");
    x = gaussSolve(Ab, b, n, 0.0001);
    printArray(x, 4, 4);

    return 0;
}