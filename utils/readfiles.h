#ifndef readfiles_h__
#define readfiles_h__

// ritorna n valori, n righe, n colonne del file
size_t* countValues(char* filename);

// From file to XYarray
// the arguments are filename and a pointer to a variable storing the length of the array
// the array returned has form array[2][length], for x and y couples
float** toXYArray(char* filename, size_t *length);

float** toArray(char* filename, size_t *rows, size_t *cols);

float* extractCol(float** array, size_t rows, size_t cols, size_t n);

#endif