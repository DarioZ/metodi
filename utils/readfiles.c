#include<stdio.h>
#include<stdlib.h>
#include"readfiles.h"

size_t* countValues(char* filename) {
    float tmp;
    char ch;
    static size_t vals[3] = {0,0,0};
    //int* vals = calloc(3, sizeof(int));

    FILE* fp = fopen(filename, "r");
    while((ch = fgetc(fp))!=EOF && ch!='\n') {
        vals[0]+=fscanf(fp, "%f", &tmp);
    }
    vals[2] = vals[0];
    while((ch = fgetc(fp))!=EOF) {
        vals[0]+=fscanf(fp, "%f", &tmp);
    }
    vals[1] = vals[0]/vals[2];
    fclose(fp);

    return vals;
}

float** toXYArray(char* filename, size_t *length){
    float f,m; //tmp variables to countlines
    int i;
    int n=0;
    float** array = malloc(2*sizeof(float*));

    FILE* fp = fopen(filename, "r");
    while(fscanf(fp, "%f%f\n",&f,&m)!=EOF) n++; //countlines
    rewind(fp);
    
    // array allocation
    array[0] = malloc(n * sizeof(float));
    array[1] = malloc(n * sizeof(float));

    for(i=0; i<n; i++){
        fscanf(fp, "%f%f\n", &array[0][i], &array[1][i]);
    }
    fclose(fp);

    *length = n;
    return array;
}

float** toArray(char* filename, size_t *rows, size_t *cols){
    size_t i, j;
    size_t* size = countValues(filename);

    // array allocation
    float** array = malloc(size[1]*sizeof(float*));
    for(i=0;i<size[1];i++) {
        array[i] = malloc(size[2] * sizeof(float));
    }

    FILE* fp = fopen(filename, "r");
    for(i=0;i<size[1];i++){
        for(j=0;j<size[2];j++) {
            fscanf(fp, "%f", &array[i][j]);
        }
    }
    fclose(fp);

    *rows = size[1];
    *cols = size[2];
    return array;
}

float* extractCol(float** array, size_t rows, size_t cols, size_t n) {
    size_t i;
    float* col = malloc(rows*sizeof(float));
    
    if(n<cols) {
        for(i=0;i<rows;i++) col[i] = array[i][n];
    } else {
        fprintf(stderr, "La colonna %d non esiste! Ci sono %d colonne.\n");
        exit(EXIT_FAILURE);
    }
    
    return col;
}